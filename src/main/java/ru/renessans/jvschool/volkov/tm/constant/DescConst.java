package ru.renessans.jvschool.volkov.tm.constant;

public interface DescConst {

    String HELP = "вывод списка команд";

    String VERSION = "вывод версии программы";

    String ABOUT = "вывод информации о разработчике";

    String INFO = "вывод информации о системе";

    String ARGUMENTS = "вывод списка поддерживаемых аргументов";

    String COMMANDS = "вывод списка поддерживаемых терминальных команд";

    String EXIT = "закрыть приложение";

    String TASK_CREATE = "добавить новую задачу";

    String TASK_LIST = "вывод списка задач";

    String TASK_CLEAR = "очистить все задачи";

    String PROJECT_CREATE = "добавить новый проект";

    String PROJECT_LIST = "вывод списка проектов";

    String PROJECT_CLEAR = "очистить все проекты";

}