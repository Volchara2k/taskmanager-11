package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String label);

    void create(String label, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> tasks();

    void clear();

}
