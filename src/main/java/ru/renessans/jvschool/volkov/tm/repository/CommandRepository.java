package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public final class CommandRepository implements ICommandRepository {

    private final Map<CommandType, List<Command>> commands = new HashMap<>();

    @Override
    public void addCommandTypes(final CommandType... commandTypes) {
        if (Objects.isNull(commandTypes)) return;

        for (CommandType commandType : commandTypes) {
            this.commands.put(commandType, new ArrayList<>());
        }
    }

    @Override
    public void addCommands(final CommandType commandType, final Command... commands) {
        if (Objects.isNull(commandType)) return;
        if (isNoCommandType(commandType)) return;

        final List<Command> commandList = this.commands.get(commandType);
        commandList.addAll(Arrays.asList(commands));
    }

    @Override
    public String getNotifyByType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) return null;
        if (ValidRuleUtil.isNullOrEmpty(command)) return null;

        final List<Command> commands = commandsByType(commandType);
        if (ValidRuleUtil.isNullOrEmpty(commands)) return null;

        setNotifiesForLists();
        for (Command cmd : commands) {
            if (Objects.equals(commandByType(commandType, cmd), command))
                return cmd.getNotification();
        }
        return null;
    }

    private boolean isNoCommandType(final CommandType commandType) {
        return this.commands.get(commandType) == null;
    }

    private void setNotifiesForLists() {
        final AtomicReference<String> commonNotify = new AtomicReference<>("");
        commandsByType(CommandType.COMMON).forEach(command -> {
            commonNotify.updateAndGet(v -> v + command.toString() + "\n");
            Command.HELP.setNotification(String.format(NotifyConst.FORMAT_MSG_HELP, commonNotify.get()));
        });
        final AtomicReference<String> argumentNotify = new AtomicReference<>("");
        commandsByType(CommandType.ARG).forEach(command -> {
            argumentNotify.updateAndGet(v -> v + command.getArgument() + "\n");
            Command.ARGUMENT.setNotification(String.format(NotifyConst.FORMAT_MSG_ARG, argumentNotify.get()));
        });
        final AtomicReference<String> commandNotify = new AtomicReference<>("");
        commandsByType(CommandType.CMD).forEach(command -> {
            commandNotify.updateAndGet(v -> v + command.getCommand() + "\n");
            Command.COMMAND.setNotification(String.format(NotifyConst.FORMAT_MSG_CMD, commandNotify.get()));
        });
    }

    private String commandByType(final CommandType commandType, final Command command) {
        if (Objects.isNull(commandType)) return null;
        if (Objects.isNull(command)) return null;

        if (commandType.isArgument()) return command.getArgument();
        if (commandType.isCommand()) return command.getCommand();
        return null;
    }

    private List<Command> commandsByType(final CommandType commandType) {
        if (Objects.isNull(commandType)) return new ArrayList<>();

        if (commandType.isCommon()) return getCommandsList(CommandType.COMMON);
        if (commandType.isArgument()) return getCommandsList(CommandType.ARG);
        if (commandType.isCommand()) return getCommandsList(CommandType.CMD);
        return new ArrayList<>();
    }

    private List<Command> getCommandsList(final CommandType commandType) {
        if (Objects.isNull(commandType)) return new ArrayList<>();
        if (isNoCommandType(commandType)) return new ArrayList<>();

        return commands.get(commandType);
    }

}