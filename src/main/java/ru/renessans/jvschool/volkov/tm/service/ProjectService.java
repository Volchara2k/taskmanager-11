package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IProjectRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String title) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;

        final Project project = new Project(title);
        this.projectRepository.add(project);
    }

    @Override
    public void create(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;
        if (ValidRuleUtil.isNullOrEmpty(description)) return;

        final Project project = new Project(title, description);
        this.projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (Objects.isNull(project)) return;

        this.projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (Objects.isNull(project)) return;

        this.projectRepository.remove(project);
    }

    @Override
    public List<Project> projects() {
        return this.projectRepository.projects();
    }

    @Override
    public void clear() {
        this.projectRepository.clear();
    }

}