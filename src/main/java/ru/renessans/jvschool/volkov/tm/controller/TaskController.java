package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ITaskController;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskService;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class TaskController implements ITaskController, NotifyConst {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        final List<Task> tasks = this.taskService.tasks();
        tasks.forEach(System.out::println);
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void clearTasks() {
        this.taskService.clear();
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void createTask() {
        System.out.println(TITLE_ADD_MSG);
        final String title = ScannerUtil.nextLine();
        System.out.println(DESCRIPTION_ADD_MSG);
        final String description = ScannerUtil.nextLine();
        this.taskService.create(title, description);
        System.out.println(SUCCESS_MSG);
    }

}