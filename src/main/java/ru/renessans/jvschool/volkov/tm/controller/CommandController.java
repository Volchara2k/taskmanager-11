package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void registrationCommandTypes() {
        this.commandService.addCommandTypes();
    }

    @Override
    public void registrationCommands() {
        this.commandService.addCommonCommands();
        this.commandService.addTerminalCommands();
        this.commandService.addArgumentCommands();
    }

    @Override
    public void printCommandNotify(final String command) {
        String notify = this.commandService.getTerminalCommandNotify(command);
        System.out.println(notify);
    }

    @Override
    public void printArgumentNotify(final String command) {
        String notify = this.commandService.getArgumentCommandNotify(command);
        System.out.println(notify);
    }

}