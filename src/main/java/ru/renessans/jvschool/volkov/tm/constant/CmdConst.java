package ru.renessans.jvschool.volkov.tm.constant;

public interface CmdConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String TASK_CREATE = "task-create";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String PROJECT_CREATE = "project-create";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

}