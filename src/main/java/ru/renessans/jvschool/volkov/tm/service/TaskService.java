package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ITaskRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskService;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.List;
import java.util.Objects;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String title) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;

        final Task task = new Task(title);
        this.taskRepository.add(task);
    }

    @Override
    public void create(final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(title)) return;
        if (ValidRuleUtil.isNullOrEmpty(description)) return;

        final Task task = new Task(title, description);
        this.taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (Objects.isNull(task)) return;

        this.taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (Objects.isNull(task)) return;

        this.taskRepository.remove(task);
    }

    @Override
    public List<Task> tasks() {
        return this.taskRepository.tasks();
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

}