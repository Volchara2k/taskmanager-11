package ru.renessans.jvschool.volkov.tm.model;

import ru.renessans.jvschool.volkov.tm.constant.ArgConst;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.constant.DescConst;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.util.SystemMonitorUtil;

public enum Command implements NotifyConst {

    VERSION(CmdConst.VERSION, ArgConst.VERSION, DescConst.VERSION, VERSION_MSG),

    ABOUT(CmdConst.ABOUT, ArgConst.ABOUT, DescConst.ABOUT, String.format(FORMAT_MSG_ABOUT, DEV_MSG, DEV_MAIL_MSG)),

    INFO(CmdConst.INFO, ArgConst.INFO, DescConst.INFO, SystemMonitorUtil.getInstance().formatInfo()),

    EXIT(CmdConst.EXIT, null, DescConst.EXIT, EXIT_MSG),

    HELP(CmdConst.HELP, ArgConst.HELP, DescConst.HELP),

    ARGUMENT(CmdConst.ARGUMENTS, ArgConst.ARGUMENTS, DescConst.ARGUMENTS),

    COMMAND(CmdConst.COMMANDS, ArgConst.COMMANDS, DescConst.COMMANDS),

    TASK_CREATE(CmdConst.TASK_CREATE, null, DescConst.TASK_CREATE, TASK_CREATE_MSG),

    TASK_LIST(CmdConst.TASK_LIST, null, DescConst.TASK_LIST, TASK_LIST_MSG),

    TASK_CLEAR(CmdConst.TASK_CLEAR, null, DescConst.TASK_CLEAR, TASK_CLEAR_MSG),

    PROJECT_CREATE(CmdConst.PROJECT_CREATE, null, DescConst.PROJECT_CREATE, PROJECT_CREATE_MSG),

    PROJECT_LIST(CmdConst.PROJECT_LIST, null, DescConst.PROJECT_LIST, PROJECT_LIST_MSG),

    PROJECT_CLEAR(CmdConst.PROJECT_CLEAR, null, DescConst.PROJECT_CLEAR, PROJECT_CLEAR_MSG);

    private String command = "";

    private String argument = "";

    private String description = "";

    private String notification = "";

    Command(final String command, final String argument, final String description) {
        this.command = command;
        this.argument = argument;
        this.description = description;
    }

    Command(final String command, final String argument, final String description, final String notification) {
        this.command = command;
        this.argument = argument;
        this.description = description;
        this.notification = notification;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(final String command) {
        this.command = command;
    }

    public String getArgument() {
        return this.argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getNotification() {
        return this.notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.command != null && !this.command.isEmpty())
            result.append("Терминальная команда: ").append(this.command);
        if (this.argument != null && !this.argument.isEmpty())
            result.append(", программный аргумент: ").append(this.argument);
        if (this.description != null && !this.description.isEmpty())
            result.append("\n\t - ").append(this.description);
        return result.toString();
    }

}