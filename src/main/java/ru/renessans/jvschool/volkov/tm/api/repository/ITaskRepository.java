package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> tasks();

    void clear();

}