package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String label);

    void create(String label, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> projects();

    void clear();

}