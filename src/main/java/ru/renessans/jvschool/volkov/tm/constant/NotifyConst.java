package ru.renessans.jvschool.volkov.tm.constant;

public interface NotifyConst {

    String FORMAT_MSG_UNKNOWN = "Неизвестная команда: %s";

    String FORMAT_MSG_HELP = "%s";

    String FORMAT_MSG_ARG = "%s";

    String FORMAT_MSG_CMD = "%s";

    String FORMAT_MSG_INFO =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    String FORMAT_MSG_ABOUT = "%s - разработчик; \n%s - почта.";

    String VERSION_MSG = "Версия: 1.0.8.";

    String DEV_MSG = "Valery Volkov";

    String DEV_MAIL_MSG = "volkov.valery2013@yandex.ru";

    String EXIT_MSG = "Выход из приложения!";

    String NO_COMMAND_MSG = "Входная команда отсутствует!";

    String NO_COMMAND_TYPE_MSG = "Не указан тип команды!";

    String TITLE_ADD_MSG = "Введите заголовок для добавления: ";

    String DESCRIPTION_ADD_MSG = "Введите описание для добавления: ";

    String SUCCESS_MSG = "Успешно!\n";

    String TASK_CREATE_MSG = "Для создания задачи введите её заголовок или заголовок с описанием.";

    String TASK_LIST_MSG = "Текущий список задач: ";

    String TASK_CLEAR_MSG = "Производится очистка списка задач...";

    String PROJECT_CREATE_MSG = "Для создания проекта введите его заголовок или заголовок с описанием.";

    String PROJECT_LIST_MSG = "Текущий список проектов: ";

    String PROJECT_CLEAR_MSG = "Производится очистка списка проектов...";

}