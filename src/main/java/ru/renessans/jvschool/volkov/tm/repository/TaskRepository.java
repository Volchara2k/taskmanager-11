package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ITaskRepository;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        this.tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        this.tasks.remove(task);
    }

    @Override
    public List<Task> tasks() {
        return this.tasks;
    }

    @Override
    public void clear() {
        this.tasks.clear();
    }

}