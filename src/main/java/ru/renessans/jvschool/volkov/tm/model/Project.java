package ru.renessans.jvschool.volkov.tm.model;

import java.util.UUID;

public class Project {

    private long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;

    private String title = "";

    private String description = "";

    public Project() {
    }

    public Project(final String title) {
        this.title = title;
    }

    public Project(final String title, final String description) {
        this.title = title;
        this.description = description;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (this.title != null)
            result.append("Заголовок проекта: ").append(title);
        if (this.description != null)
            result.append(", описание проекта: ").append(this.description);
        return result.toString();
    }

}