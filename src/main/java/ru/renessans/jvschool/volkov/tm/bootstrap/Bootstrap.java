package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.controller.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.controller.IProjectController;
import ru.renessans.jvschool.volkov.tm.api.controller.ITaskController;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IProjectRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ITaskRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectService;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskService;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.controller.CommandController;
import ru.renessans.jvschool.volkov.tm.controller.ProjectController;
import ru.renessans.jvschool.volkov.tm.controller.TaskController;
import ru.renessans.jvschool.volkov.tm.repository.CommandRepository;
import ru.renessans.jvschool.volkov.tm.repository.ProjectRepository;
import ru.renessans.jvschool.volkov.tm.repository.TaskRepository;
import ru.renessans.jvschool.volkov.tm.service.CommandService;
import ru.renessans.jvschool.volkov.tm.service.ProjectService;
import ru.renessans.jvschool.volkov.tm.service.TaskService;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public Bootstrap() {
        this.commandController.registrationCommandTypes();
        this.commandController.registrationCommands();
    }

    public void run(String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(args);
    }

    private void terminalCommandPrintLoop() {
        String command = "";
        while (!CmdConst.EXIT.equals(command)) {
            command = ScannerUtil.nextLine();
            this.commandController.printCommandNotify(command);
            executeTaskOrProject(command);
        }
    }

    private void argumentPrint(String... args) {
        final String arg = args[0];
        this.commandController.printArgumentNotify(arg);
    }

    private void executeTaskOrProject(final String factor) {
        if (ValidRuleUtil.isNullOrEmpty(factor)) return;

        switch (factor) {
            case CmdConst.TASK_CREATE:
                this.taskController.createTask();
                break;
            case CmdConst.TASK_LIST:
                this.taskController.showTasks();
                break;
            case CmdConst.TASK_CLEAR:
                this.taskController.clearTasks();
                break;
            case CmdConst.PROJECT_CREATE:
                this.projectController.createProject();
                break;
            case CmdConst.PROJECT_LIST:
                this.projectController.showProjects();
                break;
            case CmdConst.PROJECT_CLEAR:
                this.projectController.clearProjects();
                break;
            default:
                break;
        }
    }

}