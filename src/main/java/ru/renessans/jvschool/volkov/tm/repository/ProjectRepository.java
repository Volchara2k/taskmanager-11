package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IProjectRepository;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        this.projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        this.projects.remove(project);
    }

    @Override
    public List<Project> projects() {
        return this.projects;
    }

    @Override
    public void clear() {
        this.projects.clear();
    }

}