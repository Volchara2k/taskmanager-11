package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.controller.IProjectController;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectService;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;

import java.util.List;

public final class ProjectController implements IProjectController, NotifyConst {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        final List<Project> projects = this.projectService.projects();
        projects.forEach(System.out::println);
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void clearProjects() {
        this.projectService.clear();
        System.out.println(SUCCESS_MSG);
    }

    @Override
    public void createProject() {
        System.out.println(TITLE_ADD_MSG);
        final String title = ScannerUtil.nextLine();
        System.out.println(DESCRIPTION_ADD_MSG);
        final String description = ScannerUtil.nextLine();
        this.projectService.create(title, description);
        System.out.println(SUCCESS_MSG);
    }

}