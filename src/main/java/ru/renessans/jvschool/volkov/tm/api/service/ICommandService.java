package ru.renessans.jvschool.volkov.tm.api.service;

public interface ICommandService {

    void addCommandTypes();

    void addCommonCommands();

    void addTerminalCommands();

    void addArgumentCommands();

    String getTerminalCommandNotify(String command);

    String getArgumentCommandNotify(String command);

}