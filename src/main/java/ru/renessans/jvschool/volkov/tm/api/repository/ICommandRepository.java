package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.model.Command;

public interface ICommandRepository {

    void addCommandTypes(CommandType... commandTypes);

    void addCommands(CommandType commandType, Command... commands);

    String getNotifyByType(CommandType commandType, String command);

}